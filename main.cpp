#include <algorithm> 
#include <array>
#include <ctype.h>
#include <iostream>
#include <string>

using namespace std;
//String conversion helper function. Takes a string, array and counter. Outputs the converted string 
string CloseBracket(string, bool[], unsigned);
//Basic string conversion logic. Takes a string as input. Displays the processed stream 
string ConvertToBracket(string);//deprecated
string BetterConvertion(string);

int main()
{
    string str;
    cout << "Введите строку: \n";
    getline( cin, str);
    
    transform(
        str.begin(),
        str.end(),
        str.begin(), 
        [](unsigned char c) { return tolower(c);} 
    );
    
    string deprecatedConvertStr = str;
    
    str = BetterConvertion(str);
    deprecatedConvertStr = ConvertToBracket(deprecatedConvertStr);
    
    cout <<"\n str output: "<< str;
    cout <<"\n deprecatedConvertStr output: "<< deprecatedConvertStr;
    
    return 0;
}

string BetterConvertion(string str) 
{
    bool bBracketStates[str.length()] = { 0 };//массив булек, указывающий на повторяемость символа: 0 - не повторялось
    bool bRepeated = 0;//повторяющийся ли символ
    //цикл идет с начала массива
    for (unsigned i = 0; i < str.length(); ++i)
    {
        cout << "\n" << str[i];
        //цикл идет с конца массива
        for (unsigned j = str.length(); j != i - 1; --j)
        {
            //проверяет только те элементы, которые не повторялись
            if (bBracketStates[i] == 0)
            {
                //проверяет, не повторился ли элемент
                if (bBracketStates[j] == 0 && str[i] == str[j] && i != j)
                {
                    bRepeated = 1;
                    str[j] = ')';
                    bBracketStates[j] = 1;
                    cout << "\n i != j: " << j;
                }
                //если цикл дошел до исходного символа
                if (i == j)
                {
                    str[i] = bRepeated ? ')' : '(';
                    cout << "\n i == j: " << j << " str[j] " << str[i];
                    if (bRepeated)
                        bRepeated = 0;
                }
            }
        }
    }
    return str;
}

string CloseBracket(string str, bool arr[], unsigned i) 
{
    bool bCondition;
    
    for (unsigned k = str.length() - 1; k != i - 1; --k)
    {
        bCondition = (arr[k] == 0) && (str[k] == str[i]);
        
        if (bCondition)
        {
            arr[k] = 1;
            str[k] = ')';
        }
    }
    
    return str;
}

string ConvertToBracket(string str)
{
    unsigned size = str.length();
    bool arr[size] = { 0 };
    bool bCondition;
    
    for (unsigned i = 0; i < str.length(); ++i)
    {
        for (unsigned j = i + 1; j < str.length(); ++j)
        {
            bCondition = (str[i] == str[j]) && (arr[j]==0);
            
            if (bCondition)
                str = CloseBracket(str, arr, i);
        }
        
        if (arr[i] == 0)
        {
            arr[i] = 1;
            str[i] = '(';
        }
    }
    
    return str;
}
